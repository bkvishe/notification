<?php

namespace App\Http\Controllers;

use Choicetechlab\Notification\Push;

class ExampleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index()
    {
        echo Push::send('test', 'firebase');
    }
}
